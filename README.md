The program collects vacancies from the site hh.ru. 
The user enters a Vacancy, City/Region/The country where he is looking for a job. A link to the Resume. Login and Password from HeadHunter, if you specified a link to the resume. 
Data from vacancies are generated in a CSV table (Salary/Position/Key skills/Address/Link to the vacancy). 
Every day the program reports which vacancies have closed/opened and adds /removes a space in the resume (if you passed a link to the vacancy, username and password) in the about field - raises the resume to the top.

Программа собирает вакансии с сайта hh.ru. 
Пользователь вводит Вакансию, Город/Регион/Страну, где ищет вакансию.
Ссылку на Резюме.
Логин и Пароль от  HeadHunter, если указали ссылку на рюземе. 
Данные из вакансий формируются в CSV таблице (Зарплата/Должность/Ключевые навыки/Адрес/Ссылка на вакансию). 
Каждые сутки программа сообщает какие вакансии закрылись/открылись и добавляет/удаляет пробел в резюме(если вы передали ссылку на вакансию, логин и пароль) в поле о себе - поднимает резюме в топ.