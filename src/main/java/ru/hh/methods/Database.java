package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.sql.*;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDate.now;


public class Database {
    private static final String DB_USERNAME = "ilua";
    private static final String DB_PASSWORD = "password";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/collection_of_vacancies";
    private static final String[] vacancyXPATH = {"//h1[@data-qa='vacancy-title']","//h1[@class=\"-SKl4SE___header-vacancy-title\"]"};
    private static final String[] addressXPATH ={"//span[@data-qa='vacancy-view-raw-address']","//p[@data-qa='vacancy-view-location']"};
    private static final String[] keySkillsXPATH ={"//div[@class='bloko-tag-list']"};
    private static final String[] salaryXPATH = {"//div[@data-qa='vacancy-salary']","//h3['class=bUvEPcr___header-salary-title']"};

    protected String getNameTable(ChromeDriver driver){
        List<WebElement> xpathElement = driver.findElements(By.xpath("//span[@class='vacancy-serp-subscription-filter-name']"));

        String nameTable=xpathElement.get(0).getAttribute("outerText")+"_"+xpathElement.get(1).getAttribute("outerText");

        nameTable=nameTable.replaceAll(" ","_").toLowerCase().replace("c#","c_sharp").replace("c++","c_plus_plus");

        return nameTable;
    }

    protected boolean existenceTable (String table) throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
        DatabaseMetaData metaData = connection.getMetaData();
        String[] types = {"TABLE"};

        ResultSet tables = metaData.getTables(null, null, "%", types);

        boolean flag = false;
        while (tables.next()) {
            if(tables.getString("TABLE_NAME").equals(table))
                flag = true;
        }
        connection.close();
        return flag;
    }

    protected void createTable(String nameDB) throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
        Statement statement = connection.createStatement();

            statement.execute("create table "+nameDB+
                    " (id serial primary key, salary text, " +
                    "post text, key_skills text, " +
                    "address text,publication_date date,closing_date text,link text);");

        connection.close();
    }

    protected void fillingTable(ChromeDriver driver,String tableName,String vacanciesLinks) throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        Statement statement = connection.createStatement();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        WebElement xpathForSalary;
        String salary = "Не указана";
        String post = "Не указан";
        String key_skills = "Не указаны";
        String address = "Не указан";

        LocalDate startSalary = now();


        driver.get(vacanciesLinks);
        int i=0;
        while (i<(vacancyXPATH.length)) {
            if (driver.findElements(By.xpath(vacancyXPATH[i])).size() > 0) {
                xpathForSalary = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(vacancyXPATH[i])));
                post = xpathForSalary.getAttribute("outerText");
                i=vacancyXPATH.length+1;
            }
            i++;
        }

        i=0;
        while (i<(addressXPATH.length)) {
            if (driver.findElements(By.xpath(addressXPATH[i])).size() > 0) {
                xpathForSalary = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(addressXPATH[i])));
                address = xpathForSalary.getAttribute("outerText");
            }
            if (!address.equals("Не указан"))
                break;
           i++;
        }






        if (driver.findElements(By.xpath("//div[@class='bloko-tag-list']")).size() > 0) {
            List<WebElement> xpathElement = driver.findElements(By.xpath("//div[@class='bloko-tag bloko-tag_inline']"));
            key_skills = (xpathElement.get(0).getAttribute("outerText").replaceAll(" ", " "));
            for (int l = 1; l < xpathElement.size(); l++) {
                key_skills += ", " + (xpathElement.get(l).getAttribute("outerText"));
            }
        }



        i=0;
        while (i<salaryXPATH.length) {
            if (driver.findElements(By.xpath(salaryXPATH[i])).size() > 0) {
                xpathForSalary = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(salaryXPATH[i])));
                salary = xpathForSalary.getAttribute("outerText").replaceAll(" ", " ").replaceAll(" ", " ");
                i= salaryXPATH.length;
            }
           i++;
        }

        String values = "('"+salary+"','"+post+"','"+key_skills+"','"
                +address+"','"+startSalary+"','"+vacanciesLinks+"');";

        statement.execute("INSERT INTO "+tableName+" (salary,post,key_skills,address,publication_date,link) VALUES "
        +values);

       connection.close();
    }

    protected void getClosingDate (ChromeDriver driver,String table) throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT link FROM "+table);


        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));

        String linkDate;
        Boolean flag;

        List<String> links =new ArrayList<>();
        while (resultSet.next()){
            links.add(resultSet.getString(1));
        }


        ResultSet resultSet1 = statement.executeQuery("SELECT closing_date FROM "+table);
        List<String> archive =new ArrayList<>();
        while (resultSet1.next()){
            archive.add(resultSet1.getString(1));
        }


        for (int i=0;i<links.size();i++){
            linkDate = links.get(i);
            driver.get(linkDate);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='supernova-logo supernova-logo_inversed supernova-logo_hh-ru\']")));
            flag = driver.findElements(By.xpath("//div[@class=\"bloko-text bloko-text_small bloko-text_strong\"]")).size()>0;
            if(flag){
                String dateArchive = driver.findElement(By.xpath("//div[@class=\"bloko-text bloko-text_small bloko-text_strong\"]")).getText().replace("&nbsp;"," ");
                String closDate = archive.get(i);
                if (closDate==null)
                statement.execute("UPDATE "+table+" SET closing_date='" + dateArchive +"' WHERE link='" + linkDate+"';");
            }
        }

        connection.close();
    }

    protected boolean existenceLineInTable(String table,String link) throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT link FROM "+table);
        Boolean flag = false;
        while (resultSet.next()) {
            if (resultSet.getString(1).equals(link))
                flag = true;
        }
        connection.close();
        return flag;
    }


    protected List<String> getAllTable () throws SQLException, IOException {
        Connection connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
        DatabaseMetaData metaData = connection.getMetaData();
        String[] types = {"TABLE"};

        ResultSet tables = metaData.getTables(null, null, "%", types);


        List<String> listOfTables = new ArrayList<>();
        while(tables.next()) {
            listOfTables.add(tables.getString("TABLE_NAME"));
        }

        connection.close();
        return listOfTables;

    }

    protected void exportSQLToCSV (String pathToFile, String tableName) throws SQLException, IOException {
        Connection connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
        Statement statement = connection.createStatement();
        String patchAndFileName = pathToFile+tableName+".csv";

        patchAndFileName = "'"+patchAndFileName+"'";
        statement.execute(" COPY " + tableName +" TO " + patchAndFileName +" WITH CSV DELIMITER ',' HEADER;");
        statement.close();
        connection.close();
        System.out.println("Database save to: "+patchAndFileName);
    }




}
