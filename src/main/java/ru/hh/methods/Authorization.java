package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;
import java.time.Duration;

public class Authorization {
    protected Cookie[] login(String login, String password) throws InterruptedException {
        var options = new ChromeOptions();
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        ChromeDriver driver = new ChromeDriver(options.addArguments("headless"));
        driver.get("https://hh.ru/applicant/resumes?hhtmFrom=resume&hhtmFromLabel=header");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));


        if ((driver.findElements(By.xpath("//button[@data-qa='expand-login-by-password']")).size() > 0) || (driver.findElements(By.xpath("//a[@data-page-analytics-event=\"loginButton\"]")).size() > 0)) {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@data-qa=\"expand-login-by-password\"]"))).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@data-qa=\"login-input-username\"]"))).clear();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@data-qa=\"login-input-username\"]"))).sendKeys(login);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@data-qa=\"login-input-password\"]"))).clear();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@data-qa=\"login-input-password\"]"))).sendKeys(password);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class=\"bloko-button bloko-button_kind-primary\"]"))).click();
        }

        Thread.sleep(2500);
        boolean checkWarningLogin = driver.findElements(By.xpath("//div[@data-qa='account-login-error']")).size() > 0;
        boolean checkEntering = driver.findElements(By.xpath("//div[@class='bloko-form-error bloko-form-error_entering']")).size() > 0;
        if (checkWarningLogin) {
            driver.quit();
            return null;
        }
        if (checkEntering) {
            driver.quit();
            return null;
        }


        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='supernova-icon-dynamic']")));
        driver.navigate().refresh();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='supernova-icon-dynamic']")));

        Cookie[] cookie = driver.manage().getCookies().toArray(new Cookie[0]);


        driver.quit();

        return cookie;
    }


    protected ChromeDriver addCookies(ChromeDriver driver, Cookie[] cookies) throws InterruptedException {

        for (int i = 0; i < cookies.length; i++) {
            driver.manage().addCookie(cookies[i]);
        }
        return driver;
    }

}
