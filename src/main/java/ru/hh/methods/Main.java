package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {

    protected static List threadResumeCollect = new ArrayList();

    public static void main(String[] args) throws IOException, InterruptedException, SQLException {

        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        Logger logger = Logger.getLogger("webdriver.chrome.driver"); logger.setLevel(Level.SEVERE);


        HashMap<String, String> hashMap = new HashMap<>();
        List<String> keySkills = new ArrayList<>();


        String login, password;
        Resume resume = new Resume();

        Authorization authorization = new Authorization();
        System.out.print("Enter a login from HeadHunter: ");
        login = bufferedReader.readLine();

        System.out.print("Enter a password from HeadHunter: ");
        password = bufferedReader.readLine();

        Cookie[] cookies;
        cookies = authorization.login(login, password);
        while (cookies == null) {
            System.out.println("Login or Password not correct");

            System.out.print("Enter a login from HeadHunter: ");
            login = bufferedReader.readLine();

            System.out.print("Enter a password from HeadHunter: ");
            password = bufferedReader.readLine();

            cookies = authorization.login(login, password);
        }

        ArrayList<ParseVacancies> listParseVacancies = new ArrayList<>();
        ArrayList<ResumeInTop> listResume = new ArrayList<>();


        while (true) {
            System.out.println("1. Collect vacancies.");
            System.out.println("2. Automatic raising of the resume to the top.");
            System.out.println("3. Stop collecting vacancies.");
            System.out.println("4. Stop raising the resume to the top.");
            System.out.println("5. Export database table to CSV.");
            System.out.print("Enter the desired item: ");
            int command = Integer.parseInt(bufferedReader.readLine());

            switch (command) {

                case (1) -> {
                    System.out.print("Enter jobs: ");
                    String jobs = bufferedReader.readLine();
                    System.out.print("Enter area: ");
                    String area = bufferedReader.readLine();

                    ParseVacancies parseVacancies = new ParseVacancies(jobs, area);


                    parseVacancies.start();
                    listParseVacancies.add(parseVacancies);

                }


                case (2) -> {

                    int num = 1;


                    hashMap = resume.getHashResume(cookies);

                    System.out.println("List of available resumes:");
                    for (Map.Entry entry : hashMap.entrySet()) {
                        System.out.println("\t" + num + ". " + entry.getValue());
                        num++;
                    }
                    System.out.println("Choose a resume number to raise to the top:");
                    int numOfResume = Integer.parseInt(bufferedReader.readLine());
                    String linkToResume = (String) hashMap.keySet().toArray()[numOfResume - 1];
                    boolean flag = resume.checkAboutMe(cookies, linkToResume);
                    if (!flag) {
                        System.out.println("There is no item about yourself");
                        System.out.print("Tell us about yourself: ");
                        String aboutMe = bufferedReader.readLine();
                        ResumeInTop resumeThread = new ResumeInTop(cookies, linkToResume, aboutMe);
                        resumeThread.setName((String) hashMap.values().toArray()[numOfResume - 1]);
                        listResume.add(resumeThread);
                        resumeThread.start();
                    } else {
                        ResumeInTop resumeThread = new ResumeInTop(cookies, linkToResume);
                        resumeThread.setName((String) hashMap.values().toArray()[numOfResume - 1]);
                        listResume.add(resumeThread);
                        resumeThread.start();
                    }

                }

                case (3) -> {
                    System.out.println("Number of active job fees:");
                    for (int i = 0; i < listParseVacancies.size(); i++) {
                        System.out.println(i+1 +". " + listParseVacancies.get(i).getName());
                    }
                    System.out.print("Select a stream to stop: ");
                    int i = Integer.parseInt(bufferedReader.readLine());
                    listParseVacancies.get(i - 1).interrupt();
                    listParseVacancies.remove(i - 1);
                }

                case (4) -> {
                    System.out.println("The number of active raises to the top: ");
                    for (int i = 0; i < listResume.size(); i++) {
                        System.out.println(i+1+" "+listResume.get(i).getName());
                    }
                    System.out.println("Select a stream to stop: ");
                    int i = Integer.parseInt(bufferedReader.readLine());
                    listResume.get(i - 1).interrupt();
                    listResume.remove(i - 1);
                }

                case (5) -> {
                    Database database = new Database();
                    List<String> listOfTables  = database.getAllTable();

                    System.out.println("Enter path to save (Example :/Users/Admin/Documents/): ");
                    String put = bufferedReader.readLine();
                    for(int i=0;i<listOfTables.size();i++){
                        System.out.println(i+1+". "+listOfTables.get(i));
                    }
                    System.out.print("Chose table:  ");
                    int index = Integer.parseInt(bufferedReader.readLine());
                    if((index<1)||(index>listOfTables.size())){
                        System.err.println("There is no such table.");
                        break;
                    }
                    else
                    database.exportSQLToCSV(put,listOfTables.get(index-1));
                }


                default -> {
                    System.err.println("The command is not initialized.");
                    break;
                }

            }
        }
    }
}


