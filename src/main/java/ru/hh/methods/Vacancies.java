package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Vacancies extends Thread {

    ChromeDriver driver;
    String jobs;
    String area;

    Vacancies(ChromeDriver driver,String jobs,String area){
        this.driver = driver;
        this.jobs = jobs;
        this.area = area;
    }

    protected ChromeDriver openPageVacancies()  {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        driver.get("https://hh.ru/");

        String searchTerm = this.jobs + " " + this.area;
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@data-qa='search-input']"))).sendKeys(searchTerm);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-qa='search-button']"))).click();
        return driver;
    }

    protected List<String> GetLinks(ChromeDriver driver){
        List<WebElement> xpathElement = driver.findElements(By.xpath("//a[@data-qa='serp-item__title']"));
        List<String> vacanciesLinks = new ArrayList<>();
        for (int i = 0; i < xpathElement.size(); i++){
            vacanciesLinks.add(xpathElement.get(i).getAttribute("href"));
        }
        return vacanciesLinks;
    }

    public synchronized void run() {

        Vacancies vacancies = new Vacancies(driver,jobs,area);
        Database database = new Database();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        driver = vacancies.openPageVacancies();
        String tableName = database.getNameTable(driver);

        Thread.currentThread().setName(tableName);

        boolean existenceTable = false;
        try {
            existenceTable = database.existenceTable(tableName);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if (existenceTable==false) {
            if (!isInterrupted()){
                try {
                    database.createTable(tableName);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
        }
            else {
                try {
                    throw new InterruptedException();
                } catch (InterruptedException e) {
                    driver.quit();
                    System.out.println("The thread is stopped.н");
                }
            }
        }


        List<String> links = new ArrayList<>();
        String url;



        links.addAll(vacancies.GetLinks(driver));
        Set<String> set = new HashSet<>(links);
        links.clear();
        links.addAll(set);

        boolean isPresent = driver.findElements(By.xpath("//a[@data-qa='pager-next']")).size() > 0;
        while (isPresent) {
            if (!isInterrupted()) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@data-qa='pager-next']"))).click();
                url = driver.getCurrentUrl();
                links.addAll(vacancies.GetLinks(driver));
                driver.get(url);
                isPresent = driver.findElements(By.xpath("//a[@data-qa='pager-next']")).size() > 0;
            }
            else {
                try {
                    throw new InterruptedException(); //бросаем исключение в таких случаях как этот (циклы)
                } catch (InterruptedException e) {
                    driver.quit();
                    System.out.println("The thread is stopped.");
                }
            }
        }
        int size = links.size();
        int i =0;
        while (i<size) {
            if (!isInterrupted()){
                boolean flag = false;
            try {
                flag = database.existenceLineInTable(tableName, links.get(i));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            if (flag != true) {
                try {
                    database.fillingTable(driver, tableName, links.get(i));
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            else{

            }
            i++;
        }
            else {
                try {
                    throw new InterruptedException(); //бросаем исключение в таких случаях как этот (циклы)
                } catch (InterruptedException e) {
                    driver.quit();
                    System.out.println("The thread is stopped.");
                }
            }
        }

    driver.quit();
    }

}
