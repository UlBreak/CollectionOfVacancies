package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import javax.xml.xpath.XPath;
import java.io.*;
import java.sql.Driver;
import java.time.Duration;
import java.util.*;


public class Resume extends Thread {


    public Resume() {

    }

    protected HashMap<String, String> getHashResume(Cookie[] cookies) throws InterruptedException {
        HashMap<String, String> hashMap = new HashMap();
        ChromeDriver driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        Authorization authorization = new Authorization();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        driver.get("https://hh.ru/applicant/resumes?hhtmFrom=resume&hhtmFromLabel=header");
        driver = authorization.addCookies(driver, cookies);
        driver.navigate().refresh();
        String key, value;


        ////a[@data-qa='resume-title-link'] - ссылка
        //span[@data-qa='resume-title'] - наименование
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@data-qa='resume-title']")));
        List<WebElement> xpathElementForTitle = driver.findElements(By.xpath("//span[@data-qa='resume-title']"));
        List<WebElement> xpathElementForLink = driver.findElements(By.xpath("//a[@data-qa='resume-title-link']"));

        for (int i = 0; i < xpathElementForTitle.size(); i++) {
            key = xpathElementForLink.get(i).getAttribute("href");
            value = xpathElementForTitle.get(i).getAttribute("outerText");
            hashMap.put(key, value);
        }

        driver.quit();
        return hashMap;
    }

    protected List<String> getStatusResume(Cookie[] cookies) throws InterruptedException {
        List<String> statusResume = new LinkedList<>();
        ChromeDriver driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        Authorization authorization = new Authorization();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        driver.get("https://hh.ru/applicant/resumes?hhtmFrom=resume&hhtmFromLabel=header");
        driver = authorization.addCookies(driver, cookies);
        driver.navigate().refresh();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@data-qa='resume-title']")));
        List<WebElement> xpathElementForTitle = driver.findElements(By.xpath("//span[@data-qa='resume-title']"));
        List<WebElement> xpathElementForStatus = driver.findElements(By.xpath("//div[@class='applicant-resumes-status']"));
        List<String> resume = new ArrayList<>();
        List<String> status = new ArrayList<>();

        for (int i = 0; i < xpathElementForTitle.size(); i++) {
            resume.add(xpathElementForTitle.get(i).getAttribute("outerText").replaceAll("\n", " "));
        }
        for (int i = 0; i < xpathElementForStatus.size(); i++) {
            status.add(xpathElementForStatus.get(i).getAttribute("outerText").replaceAll("\n", " "));
        }
        int resumeSize = resume.size();
        int statusSize = status.size();
        for(int i=0;i<statusSize;i++){
            statusResume.add(resume.get(i)+" = "+status.get(i));
        }
        if(resumeSize>statusSize) {
            for (int i = statusSize; i < resumeSize; i++) {
                statusResume.add(resume.get(i));
            }
        }


        driver.quit();
        return statusResume;

    }

    protected void allStatusResume(ChromeDriver driver) {
        List<WebElement> textResumeVisibility = driver.findElements(By.xpath("//label[@class='bloko-radio']"));
        List<WebElement> statusResumeVisibility = driver.findElements(By.xpath("//input[@class='bloko-radio__input']"));
        for (int i = 0; i < textResumeVisibility.size(); i++) {
            System.out.print(i + 1 + ". ");
            System.out.print(textResumeVisibility.get(i).getText().replace("\n", " "));
            boolean flags = Boolean.parseBoolean(statusResumeVisibility.get(i).getAttribute("checked"));
            if (flags) {
                System.out.println(" - Active");
            } else {
                System.out.println(" - Not active");
            }
        }


        List<WebElement> statusAnonymousResume = driver.findElements(By.xpath("//input[@class='bloko-checkbox__input']"));

        String statusAnResume = statusAnonymousResume.get(0).getAttribute("checked");
        if (statusAnResume != null) {
            System.out.println("6. Анонимное резюме - Active");
        } else
            System.out.println("6. Анонимное резюме - Not active");
    }

    protected void anonymousStatusResume(ChromeDriver driver) {
        List<WebElement> textAnonymousResume = driver.findElements(By.xpath("//div[@class='bloko-form-item']"));
        List<WebElement> statusAnonymousResume = driver.findElements(By.xpath("//input[@class='bloko-checkbox__input']"));
        for (int i = 0; i < textAnonymousResume.size(); i++) {
            System.out.print("\t" + (i + 1) + ". ");
            System.out.print(textAnonymousResume.get(i).getText());
            String statusAnonymousResumeParam = statusAnonymousResume.get(i + 1).getAttribute("checked");
            if (statusAnonymousResumeParam != null)
                System.out.println(" - Active");
            else
                System.out.println(" - Not active");
        }
    }



    protected void changeVisibitlyCompany(ChromeDriver driver) throws InterruptedException, IOException {

        Scanner scanner = new Scanner(System.in);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(25));
        int statusNum;


        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        Boolean newFlag = true;


        if (driver.findElements(By.xpath("//div[@class=\"bloko-modal-header\"]")).size() < 1) {

            boolean whitelistChange = driver.findElements(By.xpath("//button[@data-qa='resume-visibility-employers-whitelist-change']")).size()>0;
            boolean whitelistAdd = driver.findElements(By.xpath("//button[@data-qa='resume-visibility-employers-whitelist-add']")).size()>0;
            boolean visibilityBlacklistChange = driver.findElements(By.xpath("//button[@data-qa='resume-visibility-employers-blacklist-change']")).size()>0;
            boolean visibilityBlacklistAdd = driver.findElements(By.xpath("//button[@data-qa='resume-visibility-employers-blacklist-add']")).size()>0;

            if (whitelistChange) {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@data-qa='resume-visibility-employers-whitelist-change']"))).click();
            }
            if (whitelistAdd) {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@data-qa='resume-visibility-employers-whitelist-add']"))).click();
            }
            if(visibilityBlacklistChange){
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@data-qa='resume-visibility-employers-blacklist-change']"))).click();
            }
            if(visibilityBlacklistAdd){
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@data-qa='resume-visibility-employers-blacklist-add']"))).click();
            }
        }

        while (newFlag) {
            System.out.println("1. Add a new company\n" +
                    "2. View already added companies\n" +
                    "3. Delete the company(s)\n" +
                    "4. Save the changes and go back");
            statusNum = scanner.nextInt();

            switch (statusNum) {
                case (1) -> {
                    boolean flagForAddCompany = true;

                    System.out.print("Enter the company name: ");
                    String company = bufferedReader.readLine();


                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@placeholder='Название компании']"))).sendKeys(company);



                    boolean checkForNext = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class=\"employer-list-item\"]")));
                    WebElement buttonAddCompany = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='bloko-button bloko-button_kind-secondary']")));

                    if (checkForNext) {

                        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class=\"employer-list-item\"]")));
                        List<WebElement> choiceCompany = driver.findElements(By.xpath("//div[@class=\"employer-list-item\"]"));
                        List<WebElement> statusChoiceCompany = driver.findElements(By.xpath("//div[@class='employer-list-item']/label/input[@type='checkbox']"));
                        List<WebElement> choiceCompanyForClick = driver.findElements(By.xpath("//div[@class=\"employer-list-item\"]/label/span[@class=\"bloko-checkbox__text\"]"));


                        while (flagForAddCompany) {
                            System.out.println("Possible options: ");
                            for (int i = 0; i < choiceCompany.size(); i++) {
                                System.out.print((i + 1) + ". " + choiceCompany.get(i).getText().replace("\n", " - "));
                                String statusAnonymousResumeParam = statusChoiceCompany.get(i).getAttribute("checked");

                                if (statusAnonymousResumeParam != null)
                                    System.out.println(" - Active");
                                else
                                    System.out.println(" - Not active");
                            }
                            System.out.println((choiceCompany.size() + 1) + ". Save and go back");
                            System.out.print("Select the desired item: ");
                            int checkBox = scanner.nextInt();
                            if ((checkBox - 1) != choiceCompany.size()) {
                                choiceCompanyForClick.get(checkBox - 1).click();
                                Thread.sleep(1000);
                            } else {
                                buttonAddCompany.click();
                                flagForAddCompany = false;
                            }
                        }
                    } else {
                        System.out.println("No such company was found");

                    }

                }
                case (2) -> {
//                                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@data-qa='resume-visibility-employers-whitelist-change']"))).click();
                    System.out.println("Already selected companies: ");
                    List<WebElement> activeCompany = driver.findElements(By.xpath("//div[@class='bloko-tag']"));
                    for (int i = 0; i < activeCompany.size(); i++) {
                        System.out.println((i + 1) + ". " + activeCompany.get(i).getText());
                    }
                }
                case (3) -> {

                    boolean flagForDeleteCompany = true;
                    while (flagForDeleteCompany) {
                        System.out.println("Select a company to delete: ");
//                                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='bloko-tag']")).);
                        List<WebElement> nameActiveCompany = driver.findElements(By.xpath("//div[@class='bloko-tag']"));
                        List<WebElement> deleteCompany = driver.findElements(By.xpath("//button[@class=\"bloko-tag-button\"]"));

                        int i = 0;
                        for (; i < nameActiveCompany.size(); i++) {
                            System.out.println((i + 1) + ". " + nameActiveCompany.get(i).getText());
                        }
                        System.out.println((i + 1) + ". Back");
                        System.out.print("Enter the number of the campaign to delete: ");
                        int idCompany = scanner.nextInt();
                        if ((idCompany - 1) == nameActiveCompany.size()) {
                            flagForDeleteCompany = false;
                        } else
                            deleteCompany.get(idCompany - 1).click();

                        Thread.sleep(2500);
                    }
                }
                case (4) -> {
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class=\"bloko-button bloko-button_kind-success\"]"))).click();
                    newFlag = false;
                }
                default -> System.out.println("The command is not initialized: ");
            }
        }
    }


    protected void changeStatusResume(Cookie[] cookies, String linkToResume) throws InterruptedException, IOException {

        ChromeDriver driver = new ChromeDriver();
        Scanner scanner = new Scanner(System.in);

        System.setProperty("webdriver.chrome.driver", "chromedriver");
        Authorization authorization = new Authorization();


        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(25));
        driver.get(linkToResume);
        driver = authorization.addCookies(driver, cookies);
        driver.navigate().refresh();


        String webElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@data-qa='resume-aside__visibility-link']"))).getAttribute("href");
        driver.get(webElement);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='bloko-radio__text']")));

        List<WebElement> textResumeVisibility = driver.findElements(By.xpath("//label[@class='bloko-radio']"));
        List<WebElement> anonymousResume = driver.findElements(By.xpath("//span[@class='bloko-checkbox__text']"));
        List<WebElement> saveButton = driver.findElements(By.xpath("//button[@class='bloko-button bloko-button_kind-primary']"));
        allStatusResume(driver);


        Boolean flagForMainChange = true;
        while (flagForMainChange) {
                System.out.println("Change resume status to:");
            allStatusResume(driver);
            System.out.println("7. Сохранить изменения и выйти");
            int numForChange = scanner.nextInt();
            switch (numForChange) {
                case (1) -> {
                    textResumeVisibility.get(0).click();
                }
                case (2) -> {
                    textResumeVisibility.get(1).click();
                    changeVisibitlyCompany(driver);
                }
                case (3) -> {
                    textResumeVisibility.get(2).click();
                    changeVisibitlyCompany(driver);

                }
                case (4) -> {
                    textResumeVisibility.get(3).click();
                }
                case (5) -> {
                    textResumeVisibility.get(4).click();
                }

                case (6) -> {
                    boolean flagForAnonymous = true;
                    while (flagForAnonymous) {

                        String statusAnonymousResume = driver.findElements(By.xpath("//input[@class='bloko-checkbox__input']")).get(0).getAttribute("checked");
                        String status;

                        if (statusAnonymousResume != null)
                            status = (" - Active");
                            else
                            status = (" - Not active");

                        System.out.println("1.Activate/deactivate anonymous resume" + status);
                        System.out.println("2.Change the parameters of an anonymous resume");
                        System.out.println("3.Back");
                        numForChange = scanner.nextInt();
                        switch (numForChange) {
                            case (1) -> {
                                anonymousResume.get(0).click();
                            }
                            case (2) -> {
                                boolean flagForAnonymousSubParagraph = true;
                                while (flagForAnonymousSubParagraph) {
                                    anonymousStatusResume(driver);
                                    System.out.println("\t6.Back <-");
                                    System.out.println("\t7.Save the change and exit");
                                    numForChange = scanner.nextInt();
                                    switch (numForChange) {
                                        case (1) -> {
                                            anonymousResume.get(1).click();
                                        }
                                        case (2) -> {
                                            anonymousResume.get(2).click();
                                        }
                                        case (3) -> {
                                            anonymousResume.get(3).click();
                                        }
                                        case (4) -> {
                                            anonymousResume.get(4).click();
                                        }
                                        case (5) -> {
                                            anonymousResume.get(5).click();
                                        }
                                        case (6) -> {
                                            flagForAnonymousSubParagraph = false;
                                            break;
                                        }
                                        case (7) -> {
                                            saveButton.get(0).click();

                                            flagForAnonymous = false;
                                            flagForAnonymousSubParagraph = false;
                                            flagForMainChange = false;
                                            break;
                                        }
                                    }
                                }
                                if (flagForAnonymous == false){
                                    break;
                                }

                            }
                            case (3) -> {
                                flagForAnonymous = false;
                                break;
                            }
                        }
                    }

                }
                case (7) -> {
                    saveButton.get(0).click();

                    flagForMainChange = false;
                }
            }
        }
        driver.quit();
    }


    protected boolean checkAboutMe(Cookie[] cookies,String link){
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        Authorization authorization = new Authorization();
        var options = new ChromeOptions();
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        ChromeDriver driver = new ChromeDriver(options.addArguments("headless"));
        driver.get(link);
        try {
            driver = authorization.addCookies(driver, cookies);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        driver.navigate().refresh();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        boolean checkEntering = driver.findElements(By.xpath("//span[text() = 'обо мне']")).size() > 0;
        if(checkEntering){
            return false;
        }

        return true;
    }



    Cookie[] cookies;
    String link;
    String aboutMe;
    Resume(Cookie[] cookies, String link){
        this.cookies=cookies;
        this.link=link;
    }

    Resume(Cookie[] cookies, String link,String aboutMe){
        this.cookies=cookies;
        this.link=link;
        this.aboutMe=aboutMe;
    }


    public synchronized void run() {



        var options = new ChromeOptions();
        ChromeDriver driver = new ChromeDriver(options.addArguments("headless"));
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        String textFromElement;
        Authorization authorization = new Authorization();
        driver.get(link);
        try {
            driver = authorization.addCookies(driver, cookies);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        driver.navigate().refresh();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        //
        if(!aboutMe.isEmpty()){
            List<WebElement> test = driver.findElements(By.xpath("//span[text() = 'обо мне']"));
            test.get(1).click();
            textFromElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']"))).getText();
            for (int i = 0; i != textFromElement.length(); i++) {
                if (!isInterrupted()) {
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']"))).sendKeys(Keys.BACK_SPACE);
                } else {
                    try {
                        throw new InterruptedException();
                    } catch (InterruptedException e) {
                        driver.quit();
                        System.out.println("Поток остановлен");
                    }
                }
            }



            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']"))).sendKeys(aboutMe);

        } else {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@data-qa='resume-block-skills-edit']"))).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']")));
            textFromElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']"))).getText();

            StringBuffer sB = new StringBuffer(textFromElement);
            int spaceIndex = textFromElement.indexOf(" ");


            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//textarea[@name='skills[0].string']")));


            for (int i = 0; i != textFromElement.length(); i++) {
                if (!isInterrupted()) {
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']"))).sendKeys(Keys.BACK_SPACE);
                } else {
                    try {
                        throw new InterruptedException();
                    } catch (InterruptedException e) {
                        driver.quit();
                        System.out.println("Поток остановлен");
                    }
                }
            }

            if (textFromElement.startsWith("  ", spaceIndex)) {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']"))).sendKeys(textFromElement.replace("  ", " "));
            } else {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@name='skills[0].string']"))).sendKeys(sB.insert(spaceIndex, " "));
            }

        }
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-qa='resume-submit']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@data-qa='resume-block-skills-edit']")));
        driver.quit();
    }


}
