package ru.hh.methods;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;


class ParseVacancies extends Thread {

    Database database = new Database();
    String jobs;
    String area;


    ParseVacancies(String jobs, String area) {
        this.jobs = jobs;
        this.area = area;
    }





    volatile LocalDate firstDate;
    volatile LocalDate secondDate;

    public synchronized void run() {

        BlockingQueue queue = new BlockingQueue();



        var options = new ChromeOptions();
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        ChromeDriver driver = new ChromeDriver(options.addArguments("headless"));
        Vacancies vacancies = new Vacancies(driver,jobs,area);
        driver = vacancies.openPageVacancies();
        String nameThread = database.getNameTable(driver);
        if (Main.threadResumeCollect.contains(nameThread)){
            System.out.println("Такой поток уже запушен");
            this.interrupt();
        }
        else {
            Main.threadResumeCollect.add(nameThread);
        }









        ParseVacancies.currentThread().setName(nameThread);



        this.secondDate = LocalDate.now();
        if (!isInterrupted()){
            vacancies.start();
        }
        else {
            try {
                throw new InterruptedException();
            } catch (InterruptedException e) {
                vacancies.interrupt();
                driver.quit();
                System.out.println("The thread is stopped.");
            }
        }

        try {
            if(database.existenceTable(nameThread)) {
                try {
                    database.getClosingDate(driver, nameThread);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        this.firstDate = LocalDate.now();
        while (true) {
            if (!isInterrupted()) {
                secondDate = LocalDate.now();
                if (secondDate.compareTo(firstDate) >= 1) {



                    try {
                        database.getClosingDate(driver,nameThread);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                    vacancies.start();
                    this.firstDate = secondDate;

                    try {
                        if(database.existenceTable(nameThread)) {
                            try {
                                database.getClosingDate(driver, nameThread);
                            } catch (SQLException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }

                } else {
                    Runnable task = queue.get();
                    task.run();
                }
            }
            else {
                try {
                    throw new InterruptedException();
                } catch (InterruptedException e) {
                    vacancies.interrupt();
                    driver.quit();
                    System.out.println("The thread is stopped.");
                }
                break;
            }
        }
    }
}

class BlockingQueue {
    ArrayList<Runnable> tasks = new ArrayList<>();

    public synchronized Runnable get() {
        while (tasks.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        Runnable task = tasks.get(0);
        tasks.remove(task);
        return task;
    }

    protected synchronized void put(Runnable task) {
        tasks.add(task);
        notify();
    }
}


class ResumeInTop extends Thread {
    volatile LocalDate firstDate;
    volatile LocalDate secondDate;
    volatile String linkToResume;


    Cookie[] cookies;
    String aboutMe;

    ResumeInTop(Cookie[] cookies,String linkToResume) {
        this.cookies = cookies;
        this.linkToResume = linkToResume;
    }

    ResumeInTop(Cookie[] cookies,String linkToResume,String aboutMe) {
        this.cookies = cookies;
        this.linkToResume = linkToResume;
        this.aboutMe=aboutMe;
    }


    public void run() {



        this.firstDate = LocalDate.now();
        Resume resume = new Resume(cookies, linkToResume);

        if(aboutMe.isEmpty()) {
            resume = new Resume(cookies, linkToResume, aboutMe);
        }

        BlockingQueue queue = new BlockingQueue();



        if(!isInterrupted()) {
            resume.start();
        }
        else {
            try {
                throw new InterruptedException();
            } catch (InterruptedException e) {
                System.out.println("The thread is stopped.");
            }
        }
        while (true) {
            if (!isInterrupted()){
                this.secondDate = LocalDate.now();
            if (this.secondDate.compareTo(this.firstDate) >= 1) {
                resume.start();
                this.firstDate = secondDate;
            } else {
                Runnable task = queue.get();
                task.run();
            }
        }
            else {
                try {
                    throw new InterruptedException();
                } catch (InterruptedException e) {

                    System.out.println("The thread is stopped.");
                }
            }
        }
    }
}


